<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'APIToken'], function() {
    Route::get('agencies', 'AgencyController@index');
    Route::get('agencies/{id}', 'AgencyController@show');
    Route::post('agencies', 'AgencyController@store');
    Route::put('agencies/{array}', 'AgencyController@update');
    Route::get('services', 'ServiceController@index');
    Route::get('services/{slug}', 'ServiceController@show');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
});
