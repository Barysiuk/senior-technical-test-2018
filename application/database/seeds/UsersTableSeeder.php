<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate existing records to start from scratch.
        User::truncate();

        $password = Hash::make('secret');

        User::create([
            'name' => 'Administrator',
            'email' => 'admin@thedrum.co.uk',
            'password' => $password
        ]);
    }
}
