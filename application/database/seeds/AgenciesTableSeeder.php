<?php

use App\Agency;
use Illuminate\Database\Seeder;

class AgenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate existing records to start from scratch.
        Agency::truncate();

        Agency::create([
            'agency_name' => 'RoRo\'s Rocket Chips',
            'contact_email' => 'hello@roro.com',
            'web_address' => 'http://roro.com',
            'short_description' => 'The fieriest chips known to man.',
            'established' => 2019
        ]);

        Agency::create([
            'agency_name' => 'Heavy Profesh Web Dev',
            'contact_email' => 'us@greatdevs.biz',
            'web_address' => 'https://greatdevs.biz',
            'short_description' => 'The most professional developers in town.',
            'established' => 1994
        ]);

        Agency::create([
            'agency_name' => 'Shass Kinsalott',
            'contact_email' => 'sounds@shasskinsal.ot',
            'web_address' => 'https://greatdevs.biz',
            'short_description' => 'Post-modern audio branding agency based in London.',
            'established' => 2000
        ]);

        DB::table('agency_service')->insert([
            'agency_id' => 1,
            'service_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('agency_service')->insert([
            'agency_id' => 1,
            'service_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('agency_service')->insert([
            'agency_id' => 2,
            'service_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('agency_service')->insert([
            'agency_id' => 2,
            'service_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('agency_service')->insert([
            'agency_id' => 3,
            'service_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('agency_service')->insert([
            'agency_id' => 3,
            'service_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
