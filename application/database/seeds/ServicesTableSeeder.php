<?php

use App\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate existing records to start from scratch.
        Service::truncate();

        Service::create([
            'service_name' => 'Web Development',
            'slug' => 'web-development'
        ]);

        Service::create([
            'service_name' => 'PPC',
            'slug' => 'ppc'
        ]);

        Service::create([
            'service_name' => 'SEO',
            'slug' => 'seo'
        ]);
    }
}
