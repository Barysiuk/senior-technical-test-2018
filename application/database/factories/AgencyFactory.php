<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Agency::class, function (Faker\Generator $faker) {
    return [
        'agency_name' => $faker->word,
        'contact_email' => $faker->email,
        'web_address' => $faker->url,
        'short_description' => $faker->paragraph,
        'established' => $faker->paragraph
    ];
});
