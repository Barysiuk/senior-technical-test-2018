<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Agency;
use App\Service;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithMiddleware;

class ServiceTest extends TestCase
{
    use WithFaker;

    /**
     * Retrieve an index of all services test.
     */
    public function testRetrieveIndexOfServices()
    {
        $user = factory(\App\User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('GET', '/api/services', [], $headers);
        $response->assertStatus(200);

        $response->assertJsonStructure(
           [
               [
                  'id',
                  'service_name',
                  'slug',
                  'created_at',
                  'updated_at'
               ]
           ]
        );
    }

    /**
     * View details of a single service by slug test.
     */
    public function testViewDetailsOfSingleService()
    {
        $user = factory(\App\User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('GET', '/api/services/{slug}', ['slug' => 'ppc'], $headers);
        $response->assertStatus(200);

        $this->assertEquals($response->getData()->service_name, 'PPC');
        $this->assertEquals($response->getData()->slug, 'ppc');
    }
}
