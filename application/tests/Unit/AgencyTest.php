<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Agency;
use App\Service;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithMiddleware;

class AgencyTest extends TestCase
{
    use WithFaker;

    /**
     * Create new agency test.
     */
    public function testCreateNewAgency()
    {
        $data = [
           'agency_name' => 'Test',
           'contact_email' => 'test@test.com',
           'web_address' => 'https://www.test.com',
           'short_description' => 'Created for testing puropses',
           'established' => 2018
        ];

        $user = factory(\App\User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('POST', '/api/agencies', $data, $headers);
        $response->assertStatus(201)
        ->assertJson(
            ['agency_name' => 'Test', 
             'contact_email' => 'test@test.com', 
             'web_address' => 'https://www.test.com', 
             'short_description' => 'Created for testing puropses', 
             'established' => 2018]);
    }

    /**
     * Retrieve an index of all agencies test.
     */
    public function testRetrieveIndexOfAgencies()
    {
        $user = factory(\App\User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('GET', '/api/agencies', [], $headers);
        $response->assertStatus(200);

        $response->assertJsonStructure(
           [
               [
                  'id',
                  'agency_name',
                  'contact_email',
                  'web_address',
                  'short_description',
                  'established',
                  'created_at',
                  'updated_at'
               ]
           ]
        );
    }

    /**
     * View details of a single agency test.
     */
    public function testViewDetailsOfSingleAgency()
    {
        $user = factory(\App\User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('GET', '/api/agencies/{id}', ['id' => 3], $headers);
        $response->assertStatus(200);

        $agency = Agency::findOrFail(3);

        $this->assertEquals($response->getData()->agency_name, $agency->agency_name);
        $this->assertEquals($response->getData()->contact_email, $agency->contact_email);
        $this->assertEquals($response->getData()->web_address, $agency->web_address);
        $this->assertEquals($response->getData()->short_description, $agency->short_description);
        $this->assertEquals($response->getData()->established, $agency->established);
    }

    /**
     * View services that agency offers test.
     */
    public function testViewServicesAgencyOffers()
    {
        $user = factory(\App\User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('GET', '/api/agencies/{id}', ['id' => 1], $headers);
        $response->assertStatus(200);

        $this->assertEquals($response->getData()->services[0]->slug, 'web-development');
    }

    /**
     * Update services that agency offers test.
     */
    public function testUpdateServicesAgencyOffers() 
    {
        $agencyData = [
           'id' => 1,
           'agency_name' => $this->faker->word,
           'contact_email' => $this->faker->email,
           'web_address' => $this->faker->url,
           'short_description' => $this->faker->paragraph,
           'established' => $this->faker->year($max = 'now')
        ];

        $servicesData = [2, 3];

        $data = [
           'agencyData' => $agencyData,
           'servicesData' => $servicesData
        ];

        $user = factory(\App\User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $response = $this->json('PUT', '/api/agencies/{array}', $data, $headers);
        $response->assertStatus(200);

        $this->assertEquals($response->getData()->services[0]->slug, 'ppc');
    }
}

