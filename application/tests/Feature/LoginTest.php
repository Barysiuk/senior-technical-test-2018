<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * Test that email and password are set on user login.
     *
     * @return void
     */
    public function testRequiresEmailAndLogin()
    {
        $this->markTestSkipped('must be revisited.');

        $this->json('POST', 'api/login')
            ->assertStatus(422)
            ->assertJson([
                'email' => ['The email field is required.'],
                'password' => ['The password field is required.'],
            ]);
    }

    /**
     * Test that user can login.
     *
     * @return void
     */
    public function testUserLoginsSuccessfully()
    {
        $this->markTestSkipped('must be revisited.');

        $user = factory(User::class)->create([
            'email' => 'test@test.com',
            'password' => bcrypt('secret'),
        ]);

        $payload = ['email' => 'test@test.com', 'password' => 'secret'];

        $this->json('POST', 'api/login', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'password',
                    'remember_token',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }
}
