<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;

class Agency extends Model
{
    protected $fillable = ['agency_name', 'contact_email', 'web_address', 'short_description', 'established'];

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }
}
