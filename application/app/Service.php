<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Agency;

class Service extends Model
{
    protected $fillable = ['service_name', 'slug'];

    public function agencies()
    {
        return $this->belongsToMany(Agency::class);
    }
}
