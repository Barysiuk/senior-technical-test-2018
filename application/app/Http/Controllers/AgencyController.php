<?php

namespace App\Http\Controllers;

use App\Agency;
use Illuminate\Http\Request;

class AgencyController extends Controller
{ 
    /**
     * Retrieve an index of agencies.
     *
     * @return mixed
     */
    public function index()
    {
        $agencies = Agency::all();

        return response()->json($agencies, 200);
    }

    /**
     * View details of single agency by retrieving it by its ID.
     *
     * @param  \Request  $request
     * @return mixed
     */
    public function show(Request $request)
    {
        $agencyId = $request->id;
        $agency = Agency::with('services')->findOrFail($agencyId);

        return response()->json($agency, 200);
    }

    /**
     * Create a new agency.
     *
     * @param  \Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $agency = Agency::create($request->all());

        return response()->json($agency, 201);
    }

    /**
     * Update an agency.
     *
     * @param  \Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        $agencyData = $request->agencyData;
        $servicesData = $request->servicesData;

        $agency = Agency::findOrFail($agencyData['id']);
        $agency->update($agencyData);

        if(isset($servicesData) && !empty($servicesData) && is_array($servicesData)) {
            $agency->services()->sync($servicesData);
        }
        // $agency->services()->allRelatedIds()->toArray();

        $agency = Agency::with('services')->findOrFail($agencyData['id']);
        return response()->json($agency, 200);
    }
}
