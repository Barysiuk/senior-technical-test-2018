<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;


class ServiceController extends Controller
{
    /**
     * Retrieve an index of services.
     *
     * @return mixed
     */
    public function index()
    {
        $services = Service::all();

        return response()->json($services, 200);
    }

    /**
     * View details of single service by retrieving it by its slug.
     *
     * @param  \Request  $request
     * @return mixed
     */
    public function show(Request $request)
    {
        $slug = $request->slug;
        $service = Service::where('slug', '=', $slug)->firstOrFail();

        return response()->json($service, 200);
    }
}
